<?php

/* Child theme declaration */

function iut_wp_enqueue_scripts() {

    $parenthandle = 'twentynineteen-style';
    $theme        = wp_get_theme();

    // Load parent CSS
    wp_enqueue_style(
        $parenthandle,
        get_template_directory_uri() . '/style.css', // https://plop.org/wp-content/themes/twentytwenty/style.css
        array(),
        $theme->parent()->get( 'Version' )
    );

    // Load child CSS (this theme)
    wp_enqueue_style(
        'iut-style',
        get_stylesheet_uri(), // https://plop.org/wp-content/themes/plop/style.css
        array( $parenthandle ),
        $theme->get( 'Version' )
    );

}

//add_action( 'wp_enqueue_scripts', 'iut_wp_enqueue_scripts' );


function iut_custom_post_type() {
	register_post_type('recipe',
		array(
			'labels'      => array(
				'name'          => __('Recettes'),
				'singular_name' => __('Recette'),
			),
                'show_in_rest' => true,
				'public'      => true,
				'has_archive' => 'recettes',

                'rewrite' => array('slug' => 'recette')
		)
	);
}
add_action('init', 'iut_custom_post_type');




/**
 * Creation metabox
 */

 function iut_add_meta_boxes_recette( $post ) {

	add_meta_box(
		'iut_mbox_recette',                    // Unique ID
		'Infos complémentaires',              // Box title
		'iut_mbox_recette_content', 		 // Content callback, must be of type callable
		'recipe'                          	// Post type
	);

}

add_action( 'add_meta_boxes', 'iut_add_meta_boxes_recette' );

function iut_mbox_recette_content( $post ) {

	// Get meta value
	$recette_ingredients = get_post_meta(
		$post->ID,
		'recette-ingredients',
		true
	);

    echo '<div>';
    echo    '<textarea id="Ingredients" name="Ingredients" rows="5" cols="33">'.$recette_ingredients.'</textarea>';
    echo '</div>';

}

// Save post meta
function recette_save_ingredients( $post_id ) {

	if ( isset( $_POST['recette-ingredients'] ) && !empty( $_POST['recette-ingredients'] ) ) {

		update_post_meta(
			$post_id,
			'recette-ingredients',
			sanitize_text_field( $_POST['recette-ingredients'] )
		);
	}

}

add_action( 'save_post', 'recette_save_ingredients' );
